#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
enum Camera_Movement {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

// Default camera values
const GLfloat YAW = -90.0f;
const GLfloat PITCH = 0.0f;
const GLfloat SPEED = 3.0f;
const GLfloat SENSITIVTY = 0.25f;
const GLfloat ZOOM = 45.0f;

class Camera {
public:

	glm::vec3 m_Position;
	glm::vec3 m_Front;
	glm::vec3 m_Up;
	glm::vec3 m_Right;
	glm::vec3 m_WorldUp;

	GLfloat m_Yaw;
	GLfloat m_Pitch;

	GLfloat m_MovementSpeed;
	GLfloat m_MouseSensitivity;
	GLfloat m_MouseZoom;

	Camera(glm::vec3 a_Position = glm::vec3(0, 0, 0), glm::vec3 a_Up = glm::vec3(0, 1, 0), GLfloat a_Yaw = YAW, GLfloat a_Pitch = PITCH);

	glm::mat4 getViewMatrix();
	
	void updateMovement(Camera_Movement a_Dir, GLfloat a_DeltaTime);
	void updateMouse(GLfloat a_XOffset, GLfloat a_YOffset, GLboolean a_ConstrainPitch = true);
	void updateZoom(GLfloat a_YOffset);

private:
	void updateCameraVectors();
};

