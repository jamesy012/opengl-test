#pragma once

#include <gl\glew.h>

#include "Square.h"

class Transform;

class Waves {
public:
	Waves(GLuint a_Lines, GLfloat a_WaveWidth, GLboolean a_BothSides = false);
	~Waves();

	void update(float a_DeltaTime);
	void draw();

	Transform* m_Transform;

private:
	const GLfloat DAMPENING = 0.31f;
	const GLfloat Y_OFFSET = 1.0f;
	const GLfloat WAVE_SPEED = 2.0f;


	struct WaterBar {
		Square square;
		GLfloat height;
		GLfloat heightDiff;
		GLfloat speed;
	};

	struct Wave {
		GLfloat offset;
		GLfloat waveLength;
		GLfloat amplitude;
		GLfloat speed;
	};

	const GLuint NUM_OF_WAVES = 5;

	Wave* m_Waves;

	//Wave wave;
	//Wave wave2;

	WaterBar* m_Squares;
	GLuint m_NumLines = 1;
	GLfloat m_SquareWidth = 1.0f;
	GLfloat m_WaveLength = 1.0f;

	GLfloat m_MaxHeight;
	GLfloat m_MinHeight;

	GLboolean m_BothSides;

	void linkBars();
	void updateBars();
	float randomValue();
};

