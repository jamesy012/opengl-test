#include "Model.h"

#include <assimp/Importer.hpp>
//#include <assimp/scene.h> //defined in .h
#include <assimp/postprocess.h>
#include <SOIL.h>

#include <iostream>

#include "MeshNode.h"
#include "Mesh.h"


Model::Model(const GLchar* a_FileName) {
	loadModel(a_FileName);
}

Model::~Model() {
	delete m_RootMesh;

	for (int i = 0; i < textures_loaded.size(); i++) {
		glDeleteTextures(1, &textures_loaded[i].Id);
	}
}

void Model::draw() {
	m_RootMesh->draw();
}

void Model::loadModel(std::string a_FileName) {
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(a_FileName, aiProcess_Triangulate | aiProcess_FlipUVs);

	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
		std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
		return;
	}
	m_Directory = a_FileName.substr(0, a_FileName.find_last_of('/'));

	processNode(scene->mRootNode, nullptr, scene);
}

void Model::processNode(aiNode * a_AiNodeParent, MeshNode * a_MeshParent, const aiScene* a_Scene) {
	MeshNode* thisNode = new MeshNode();
	if (a_MeshParent == nullptr) {
		m_RootMesh = thisNode;
	} else {
		thisNode->setParent(a_MeshParent);
	}

	for (GLuint i = 0; i < a_AiNodeParent->mNumMeshes; i++) {
		aiMesh* modelMesh = a_Scene->mMeshes[a_AiNodeParent->mMeshes[i]];
		Mesh* mesh = processMesh(modelMesh, a_Scene);
		thisNode->addMesh(mesh);
	}

	for (GLuint i = 0; i < a_AiNodeParent->mNumChildren; i++) {
		processNode(a_AiNodeParent->mChildren[i], thisNode, a_Scene);
	}
}

Mesh* Model::processMesh(aiMesh * a_Mesh, const aiScene* a_Scene) {
	std::vector<Mesh::Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Mesh::Texture> textures;

	//process vertex
	for (GLuint i = 0; i < a_Mesh->mNumVertices; i++) {
		Mesh::Vertex vertex;
		glm::vec3 vector;

		vector.x = a_Mesh->mVertices[i].x;
		vector.y = a_Mesh->mVertices[i].y;
		vector.z = a_Mesh->mVertices[i].z;
		vertex.Position = vector;

		vector.x = a_Mesh->mNormals[i].x;
		vector.y = a_Mesh->mNormals[i].y;
		vector.z = a_Mesh->mNormals[i].z;
		vertex.Normal = vector;

		if (a_Mesh->mTextureCoords[0]) {
			glm::vec2 vec;
			vec.x = a_Mesh->mTextureCoords[0][i].x;
			vec.y = a_Mesh->mTextureCoords[0][i].y;
			vertex.TexCoords = vec;
		} else {
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
		}

		vertices.push_back(vertex);
	}
	//process indices
	for (GLuint i = 0; i < a_Mesh->mNumFaces; i++) {
		aiFace face = a_Mesh->mFaces[i];
		for (GLuint j = 0; j < face.mNumIndices; j++) {
			indices.push_back(face.mIndices[j]);
		}
	}

	//process materials
	if (a_Mesh->mMaterialIndex >= 0) {
		aiMaterial* material = a_Scene->mMaterials[a_Mesh->mMaterialIndex];
		std::vector<Mesh::Texture> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		std::vector<Mesh::Texture> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
	}

	Mesh* finalMesh = new Mesh();
	finalMesh->setMesh(vertices, indices, textures);
	return finalMesh;
}

std::vector<Mesh::Texture> Model::loadMaterialTextures(aiMaterial * mat, aiTextureType type, std::string typeName) {
	std::vector<Mesh::Texture> textures;

	for (GLuint i = 0; i < mat->GetTextureCount(type); i++) {
		aiString str;
		mat->GetTexture(type, i, &str);
		std::string path = str.C_Str();

		GLboolean skip = false;
		for (GLuint j = 0; j < textures_loaded.size(); j++) {
			if (textures_loaded[j].Path == path) {
				textures.push_back(textures_loaded[j]);
				skip = true;
				break;
			}
		}

		if (!skip) {
			Mesh::Texture texture;
			texture.Id = TextureFromFile(str.C_Str(), m_Directory);
			texture.Type = typeName;
			texture.Path = path;
			textures.push_back(texture);
		}
	}

	return textures;
}

GLint Model::TextureFromFile(const char * path, std::string directory) {
	std::string filename = std::string(path);
	filename = directory + '/' + filename;
	GLuint textureID;
	glGenTextures(1, &textureID);

	int width, height;
	unsigned char* image = SOIL_load_image(filename.c_str(), &width, &height, 0, SOIL_LOAD_RGB);

	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	SOIL_free_image_data(image);
	return textureID;
}
