#include "Shape.h"

#include <string>
#include <sstream>

#include "Shader.h"
#include "BoundingBox.h"

Shape::Shape() {
	m_Color = glm::vec4(1.0f);
	m_BoundingBox = nullptr;
}


Shape::~Shape() {
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);

	if (m_BoundingBox != nullptr) {
		delete m_BoundingBox;
		m_BoundingBox = nullptr;
	}
}

void Shape::draw() {
	checkDirty();

	//	glEnable(GL_CULL_FACE); for double sided squares

	Shader::setUniform("Color", m_Color.r, m_Color.g, m_Color.b, m_Color.a);

	loadTextures();

	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, m_Indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	unloadTextures();
}

void Shape::drawWireframe() {
	checkDirty();

	Shader::setUniform("Color", m_Color.r, m_Color.g, m_Color.b, m_Color.a);


	glBindVertexArray(VAO);
	glDrawElements(GL_LINE_STRIP, m_Indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void Shape::drawBoundingBox() {
	if (m_BoundingBox != nullptr) {
		m_BoundingBox->draw();
	}
}

void Shape::createBoundingBox() {
	if (m_BoundingBox != nullptr) {
		delete m_BoundingBox;
		m_BoundingBox = nullptr;
	}
	m_BoundingBox = new BoundingBox(this);
	m_BoundingBox->update();
}

void Shape::setDirty() {
	m_IsDirty = true;
}

void Shape::updateMesh() {
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, m_Vertices.size() * sizeof(Vertex), &m_Vertices[0], GL_STATIC_DRAW);

	if (m_BoundingBox != nullptr) {
		m_BoundingBox->update();
	}
}

void Shape::setupMesh() {
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_Indices.size() * sizeof(GLuint), &m_Indices[0], GL_STATIC_DRAW);

	updateMesh();

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) 0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, Normal));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, TexCoords));

	glBindVertexArray(0);
}

void Shape::checkDirty() {
	if (m_IsDirty) {
		m_IsDirty = false;
		updateMesh();
	}
}

void Shape::loadTextures() {
	// Bind appropriate textures
	GLuint diffuseNr = 1;
	GLuint specularNr = 1;
	for (GLuint i = 0; i < this->m_Textures.size(); i++) {
		glActiveTexture(GL_TEXTURE0 + i); // Active proper texture unit before binding
										  // Retrieve texture number (the N in diffuse_textureN)
		std::stringstream ss;
		std::string number;
		std::string name = this->m_Textures[i].Type;
		if (name == "texture_diffuse")
			ss << diffuseNr++; // Transfer GLuint to stream
		else if (name == "texture_specular")
			ss << specularNr++; // Transfer GLuint to stream
		number = ss.str();
		// Now set the sampler to the correct texture unit
		Shader::setUniform((name + number).c_str(), i);
		// And finally bind the texture
		glBindTexture(GL_TEXTURE_2D, this->m_Textures[i].Id);
	}
}

void Shape::unloadTextures() {
	// Always good practice to set everything back to defaults once configured.
	for (GLuint i = 0; i < this->m_Textures.size(); i++) {
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}
