#include "Object.h"

#include <glm\glm.hpp>

#include "Transform.h"


Object::Object(glm::vec3 a_Position, glm::vec3 a_Roation, glm::vec3 a_Scale) {
	m_Transform = new Transform();
	m_Transform->scale(a_Scale);
	m_Transform->rotate(a_Roation);
	m_Transform->translate(a_Position);
}

Object::Object(const Transform * a_Transform) {
	m_Transform = new Transform(*a_Transform);
}

Object::~Object() {
	delete m_Transform;
}
