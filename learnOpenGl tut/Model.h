#pragma once

#include <GL\glew.h>
#include <assimp\scene.h>

#include <string>
#include <vector>

#include "Mesh.h"

class MeshNode;
class Shader;

class Model{
public:
	Model(const GLchar* a_FileName);
	~Model();

	void draw();

private:
	MeshNode* m_RootMesh;
	std::string m_Directory;
	std::vector<Mesh::Texture> textures_loaded;

	void loadModel(std::string a_FileName);
	void processNode(aiNode* a_AiNodeParent, MeshNode* a_MeshParent, const aiScene* a_Scene);
	Mesh* processMesh(aiMesh* a_AiMeshParent, const aiScene* a_Scene);
	std::vector<Mesh::Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
	GLint TextureFromFile(const char * path, std::string directory);
};

