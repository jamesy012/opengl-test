#pragma once

#include <GL\glew.h>
#include <glm\glm.hpp>

class Transform;

//currently used shader
static GLuint m_ProgramInUse = 0;

class Shader {

public:
	//program ID
	GLuint m_Program;
	//basic constructor
	Shader();
	// Constructor generates the shader on the fly
	Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
	// Uses the current shader
	void use();
	// uses the current shader
	static void use(Shader* a_Shader);
	//set shader uniform with 3 floats
	static void setUniform(const char* a_Uniform, const GLfloat a_V0, const GLfloat a_V1, const GLfloat a_V2);
	//set shader uniform with 4 floats
	static void setUniform(const char* a_Uniform, const GLfloat a_V0, const GLfloat a_V1, const GLfloat a_V2, const GLfloat a_V3);
	//set shader uniform with 1 float
	static void setUniform(const char* a_Uniform, const GLfloat a_V0);
	//set shader uniform with 1 int
	static void setUniform(const char* a_Uniform, const GLuint a_V0);
	//set shader uniform with 1 int
	static void setUniform(const char* a_Uniform, const GLint a_V0);
	//set shader uniform with a matrix 4x4
	static void setUniform(const char* a_Uniform, const glm::mat4 a_V0);
	//set shader uniform with a vec2
	static void setUniform(const char* a_Uniform, const GLfloat a_V0, const GLfloat a_V1);
	//set shader uniform with a vec2
	static void setUniform(const char* a_Uniform, const GLint a_V0, const GLint a_V1);

	static void setModel(const glm::mat4 a_Model);
	static void setModel(Transform* a_Model);
	static void setProjection(const glm::mat4 a_Projection);
	static void setView(const glm::mat4 a_View);
	static void setScreenData(const glm::mat4 a_Projection, const glm::mat4 a_View);
	static void setData(const glm::mat4 a_Model, const glm::mat4 a_Projection, const glm::mat4 a_View);
	static void setData(Transform* a_Model, const glm::mat4 a_Projection, const glm::mat4 a_View);
};
