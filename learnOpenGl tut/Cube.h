#pragma once

#include "Shape.h"

class Cube : public Shape {
public:
	Cube();
	~Cube();

	enum Points {
		FrontBotLeft,
		FrontBotRight,
		FrontTopRight,
		FrontTopLeft,
		BackBotLeft,
		BackBotRight,
		BackTopRight,
		BackTopLeft
	};

	void setPosition(Points a_Point, glm::vec3 a_Pos);
};

