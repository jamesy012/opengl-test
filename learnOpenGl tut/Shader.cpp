#include "Shader.h"

#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include "Transform.h"

Shader::Shader() {
}

Shader::Shader(const GLchar * vertexPath, const GLchar * fragmentPath) {
	// 1. Retrieve the vertex/fragment source code from filePath
	std::string vertexCode;
	std::string fragmentCode;
	std::ifstream vShaderFile;
	std::ifstream fShaderFile;
	// ensures ifstream objects can throw exceptions:
	vShaderFile.exceptions(std::ifstream::badbit);
	fShaderFile.exceptions(std::ifstream::badbit);
	try {
		// Open files
		vShaderFile.open(vertexPath);
		fShaderFile.open(fragmentPath);
		std::stringstream vShaderStream, fShaderStream;
		// Read file's buffer contents into streams
		vShaderStream << vShaderFile.rdbuf();
		fShaderStream << fShaderFile.rdbuf();
		// close file handlers
		vShaderFile.close();
		fShaderFile.close();
		// Convert stream into string
		vertexCode = vShaderStream.str();
		fragmentCode = fShaderStream.str();
	} catch (std::ifstream::failure e) {
		std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
	}
	const GLchar* vShaderCode = vertexCode.c_str();
	const GLchar * fShaderCode = fragmentCode.c_str();

	// 2. Compile shaders
	GLuint vertex, fragment;
	GLint success;
	GLchar infoLog[512];
	// Vertex Shader
	vertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex, 1, &vShaderCode, NULL);
	glCompileShader(vertex);
	// Print compile errors if any
	glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertex, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	// Fragment Shader
	fragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment, 1, &fShaderCode, NULL);
	glCompileShader(fragment);
	// Print compile errors if any
	glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fragment, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	// Shader Program
	this->m_Program = glCreateProgram();
	glAttachShader(this->m_Program, vertex);
	glAttachShader(this->m_Program, fragment);
	glLinkProgram(this->m_Program);
	// Print linking errors if any
	glGetProgramiv(this->m_Program, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(this->m_Program, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}
	// Delete the shaders as they're linked into our program now and no longer necessery
	glDeleteShader(vertex);
	glDeleteShader(fragment);

}

void Shader::use() {
	glUseProgram(this->m_Program);
	m_ProgramInUse = m_Program;
}

void Shader::use(Shader * a_Shader) {
	glUseProgram(a_Shader->m_Program);
	m_ProgramInUse = a_Shader->m_Program;
}

void Shader::setUniform(const char* a_Uniform, const GLfloat a_V0, const GLfloat a_V1, const GLfloat a_V2) {
	glUniform3f(glGetUniformLocation(m_ProgramInUse, a_Uniform), a_V0, a_V1, a_V2);
}

void Shader::setUniform(const char * a_Uniform, const GLfloat a_V0, const GLfloat a_V1, const GLfloat a_V2, const GLfloat a_V3) {
	glUniform4f(glGetUniformLocation(m_ProgramInUse, a_Uniform), a_V0, a_V1, a_V2, a_V3);
}

void Shader::setUniform(const char* a_Uniform, const GLfloat a_V0) {
	glUniform1f(glGetUniformLocation(m_ProgramInUse, a_Uniform), a_V0);
}

void Shader::setUniform(const char * a_Uniform, const GLuint a_V0) {
	glUniform1i(glGetUniformLocation(m_ProgramInUse, a_Uniform), a_V0);
}

void Shader::setUniform(const char * a_Uniform, const GLint a_V0) {
	glUniform1i(glGetUniformLocation(m_ProgramInUse, a_Uniform), a_V0);
}

void Shader::setUniform(const char * a_Uniform, const glm::mat4 a_V0) {
	glUniformMatrix4fv(glGetUniformLocation(m_ProgramInUse, a_Uniform), 1, GL_FALSE, glm::value_ptr(a_V0));
}

void Shader::setUniform(const char * a_Uniform, const GLfloat a_V0, const GLfloat a_V1) {
	glUniform2f(glGetUniformLocation(m_ProgramInUse, a_Uniform), a_V0, a_V1);
}

void Shader::setUniform(const char * a_Uniform, const GLint a_V0, const GLint a_V1) {
	glUniform2i(glGetUniformLocation(m_ProgramInUse, a_Uniform), a_V0, a_V1);
}

void Shader::setModel(const glm::mat4 a_Model) {
	glUniformMatrix4fv(glGetUniformLocation(m_ProgramInUse, "model"), 1, GL_FALSE, glm::value_ptr(a_Model));
}

void Shader::setModel(Transform* a_Model) {
	setModel(a_Model->getGlobalMatrix());
}

void Shader::setProjection(const glm::mat4 a_Projection) {
	glUniformMatrix4fv(glGetUniformLocation(m_ProgramInUse, "projection"), 1, GL_FALSE, glm::value_ptr(a_Projection));
}

void Shader::setView(const glm::mat4 a_View) {
	glUniformMatrix4fv(glGetUniformLocation(m_ProgramInUse, "view"), 1, GL_FALSE, glm::value_ptr(a_View));
}

void Shader::setScreenData(const glm::mat4 a_Projection, const glm::mat4 a_View) {
	setProjection(a_Projection);
	setView(a_View);
}

void Shader::setData(const glm::mat4 a_Model, const glm::mat4 a_Projection, const glm::mat4 a_View) {
	setModel(a_Model);
	setProjection(a_Projection);
	setView(a_View);
}

void Shader::setData(Transform * a_Model, const glm::mat4 a_Projection, const glm::mat4 a_View) {
	setData(a_Model->getGlobalMatrix(), a_Projection, a_View);
}
