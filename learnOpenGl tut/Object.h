#pragma once

#include <glm/glm.hpp>


class Transform;

class Object {
public:
	Transform* m_Transform;

	Object(glm::vec3 a_Position = glm::vec3(), glm::vec3 a_Roation = glm::vec3(), glm::vec3 a_Scale = glm::vec3(1.0f));
	Object(const Transform* a_Transform);
	~Object();

};

