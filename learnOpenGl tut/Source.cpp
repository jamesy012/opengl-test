#define GLEW_STATIC
#include <gl\glew.h>
#include <GLFW\glfw3.h>
#include <SOIL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "vld.h"

#include <iostream>
#include <time.h>       /* time */

#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include "Transform.h"
#include "Font.h"
#include "Waves.h"


#define UI_USES_SCREEN_SIZE true

//screen size of m_Window
//updated when the window gets resized
glm::vec2 m_ScreenSize(800, 600);

//main window that glfw uses
//all inputs come from this window
//opengl is rendered to this window
GLFWwindow* m_Window;

//main camera of program
//movement, rotation and field of view is done within Camera
Camera m_Camera;

//counter of number of frames since the first render
GLuint m_FramesSinceStart = 0;

//delta time of program
GLfloat m_DeltaTime = 0.0f;
//time of last rendered frame
GLfloat m_LastFrameTime = 0.0f;
//number of frames since the last fps update
//used in calculation of how many ms were used on average in each second
GLuint m_Frames = 0;
//timer for knowing when a second has been used
//every frame m_DeltaTime is added to this
GLfloat m_FrameTimer = 0.0f;

//positions of the mouse last frame
glm::vec2 m_LastMousePos;
//position of the mouse this frame
glm::vec2 m_MousePos;

//is the flashlight on
bool m_FlashLightOn = true;
//is lighting on
bool m_RealLighting = false;
//is the light spinning around a model
bool m_SpinningLight = false;

//list of draw modes
enum DrawModes {
	Fill = 0,
	Line,
	Point
};
//which draw mode is currently being used
DrawModes m_DrawMode = DrawModes::Fill;

//is the mouse locked into the window and controlling the mouse
bool m_LockedMouse = false;

//position of light
glm::vec3 m_LightPos = glm::vec3(1, 1, 1);

//contains information for frame data that is rendered to the top left
std::string m_FpsCountText;
//was there an issue with the last frame
//ie: took a long time to render
bool m_FrameProblemsLastFrame = false;

//array of all keys, currently pressed keys will be true
bool m_Keys[1024];

//gets every key press and release
//has wide verity of actions
void keyCallback(GLFWwindow* a_Window, int a_Key, int a_ScanCode, int a_Action, int a_Mode) {
	//sets key in array to be true (key pressed)
	if (a_Action == GLFW_PRESS) {
		m_Keys[a_Key] = true;
	}
	//sets key in array to be false (key let go)
	if (a_Action == GLFW_RELEASE) {
		m_Keys[a_Key] = false;
	}

	//flips flashlight power when the f key is pressed
	if (a_Action == GLFW_PRESS && a_Key == GLFW_KEY_F) {
		m_FlashLightOn = !m_FlashLightOn;
	}
	//flips lighting when enter is pressed
	if (a_Action == GLFW_PRESS && a_Key == GLFW_KEY_ENTER) {
		m_RealLighting = !m_RealLighting;
	}
	//flips whether the light spins around (0,0,0) or is doing something else
	if (a_Action == GLFW_PRESS && a_Key == GLFW_KEY_L) {
		m_SpinningLight = !m_SpinningLight;
	}

	//changes the rendering mode of opengl between DrawModes when F1 is pressed
	if (a_Action == GLFW_PRESS && a_Key == GLFW_KEY_F1) {
		//goes to next 
		m_DrawMode = DrawModes(m_DrawMode + 1);

		switch (m_DrawMode) {
			default:
				m_DrawMode = DrawModes::Fill;//resets the draw mode back to 0
				//no break so it does the case 0: as well to 
			case DrawModes::Fill:
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				break;
			case DrawModes::Line:
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				break;
			case DrawModes::Point:
				glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
				break;
		}
	}

	//if the mouse is being used to control the camera then this will stop that and give the mouse back to the user
	//if the mouse is not locked into the screen then it will close the window
	if (a_Action == GLFW_PRESS && a_Key == GLFW_KEY_ESCAPE) {
		if (m_LockedMouse) {
			glfwSetInputMode(m_Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			m_LockedMouse = false;
		} else {
			glfwSetWindowShouldClose(a_Window, GL_TRUE);
		}
	}
}

//gets called when the mouse is moved in the window
//updates camera rotation based on mouse movement
void mouseCallback(GLFWwindow* a_Window, double a_XPos, double a_YPos) {
	m_LastMousePos = m_MousePos;
	m_MousePos = glm::vec2(a_XPos, a_YPos);

	GLfloat xOffset = m_MousePos.x - m_LastMousePos.x;
	GLfloat yOffset = m_LastMousePos.y - m_MousePos.y;

	if (!m_LockedMouse) {
		return;
	}

	m_Camera.updateMouse(xOffset, yOffset, true);
}

//gets called when a mouse is clicked while on the window
//currently just locks the mouse so the camera can be used
void mouseButtonCallback(GLFWwindow* a_Window, int a_Button, int a_Action, int a_Mods) {
	m_LockedMouse = true;
	glfwSetInputMode(m_Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

//gets called when the mouse has been scrolled
//updates camera zoom based on scroll
void scrollCallback(GLFWwindow* a_Window, double a_XOffset, double a_YOffset) {
	m_Camera.updateZoom(a_YOffset);
}

//gets called when window is resized
//updates window size values
void windowCallback(GLFWwindow* a_Window, int a_Width, int a_Height) {
	m_ScreenSize.x = a_Width;
	m_ScreenSize.y = a_Height;
}

//gets called when the frame-buffer size is changed (ie window resize)
//updates frame buffer to be size of screen
void framebufferSizeCallback(GLFWwindow* a_Window, int a_Width, int a_Height) {
	glViewport(0, 0, a_Width, a_Height);

}

//sets up camera movement
void doCameraMovement() {
	if (m_Keys[GLFW_KEY_W]) {
		m_Camera.updateMovement(Camera_Movement::FORWARD, m_DeltaTime);
	}
	if (m_Keys[GLFW_KEY_S]) {
		m_Camera.updateMovement(Camera_Movement::BACKWARD, m_DeltaTime);
	}
	if (m_Keys[GLFW_KEY_A]) {
		m_Camera.updateMovement(Camera_Movement::LEFT, m_DeltaTime);
	}
	if (m_Keys[GLFW_KEY_D]) {
		m_Camera.updateMovement(Camera_Movement::RIGHT, m_DeltaTime);
	}
	if (m_Keys[GLFW_KEY_SPACE]) {
		m_Camera.updateMovement(Camera_Movement::UP, m_DeltaTime);
	}
	if (m_Keys[GLFW_KEY_LEFT_CONTROL]) {
		m_Camera.updateMovement(Camera_Movement::DOWN, m_DeltaTime);
	}
}

int main() {
	srand(time(NULL));

	//************************* OPENGL START UP *************************
	//start glfw
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

	//create window
	m_Window = glfwCreateWindow(m_ScreenSize.x, m_ScreenSize.y, "First Window", nullptr, nullptr);
	if (m_Window == nullptr) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(m_Window);

	//start glew
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		std::cout << "cant Initialize GLEW";
		return -1;
	}

	//setting opengl size
	{
		int width, height;
		glfwGetFramebufferSize(m_Window, &width, &height);
		glViewport(0, 0, width, height);
	}
	//get starting mouse pos
	{
		double xpos, ypos;
		glfwGetCursorPos(m_Window, &xpos, &ypos);
		m_MousePos = glm::vec2(xpos, ypos);
	}

	//************************* CALLBACK SETUP *************************
	glfwSetKeyCallback(m_Window, keyCallback);
	glfwSetCursorPosCallback(m_Window, mouseCallback);
	glfwSetScrollCallback(m_Window, scrollCallback);
	glfwSetMouseButtonCallback(m_Window, mouseButtonCallback);
	glfwSetFramebufferSizeCallback(m_Window, framebufferSizeCallback);
	glfwSetWindowSizeCallback(m_Window, windowCallback);

	//************************* OPENGL RENDERING VARIABLES SETUP *************************
	glPointSize(10.0f);

	//lets things behind other things be behind those things and based on render order
	glEnable(GL_DEPTH_TEST);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	//allows for alpha effects, such as text and setting alpha in fragment shaders
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//removes frame cap
	//glfwSwapInterval(0);

	//************************* CAMERA/VIEW SETUP *************************
	m_Camera = Camera(glm::vec3(0, 0, 3));

	//************************* SHADER CREATION *************************

	Shader lightingShader("Shaders/shader.vert", "Shaders/shader.frag");
	Shader basicShader("Shaders/basicTexture.vert", "Shaders/basicTexture.frag");
	Shader lampShader("Shaders/shader.vert", "Shaders/lightShader.frag");
	Shader textShader("Shaders/textShader.vert", "Shaders/textShader.frag");
	Shader fadeShader = Shader("Shaders/buildUp.vert", "Shaders/buildUp.frag");
	Shader solidColorShader = Shader("Shaders/solidcolor.vert", "Shaders/solidColor.frag");

	//************************* FONT SETUP *************************
	Font font("Fonts/comicSans_32.fnt");

	//************************* MODEL LOADING *************************
	Model nanosuitModel = Model("Models/Nanosuit/nanosuit.obj");
	Model box = Model("Models/box.obj");

	//************************* GENERAL OBJECT SETUP *************************
	Waves squareWave = Waves(100, 5.0f, false);

	//for the fade shader
	//contains position of line
	float lineHeight = 0;


	//************************* TRANSFORM SETUP *************************
	Transform spinningNanosuit;
	spinningNanosuit.translate(glm::vec3(0.0f, -1.75f, 0.0f));
	spinningNanosuit.scale(glm::vec3(0.2f, 0.2f, 0.2f));

	Transform movingNanosuit = spinningNanosuit;
	movingNanosuit.translate(glm::vec3(-5, 0, 0));
	movingNanosuit.rotate(glm::vec3(0, 45, 0));

	Transform rotateAroundNanosuit;

	squareWave.m_Transform->translate(glm::vec3(-1, 0, -5));

	//************************* SHADER SETUP *************************

	//.use must be used before setting uniforms
	lightingShader.use();

	//directional light
	lightingShader.setUniform("dirLight.direction", -0.2f, -0.1f, -0.3f);
	lightingShader.setUniform("dirLight.ambient", 0.05f, 0.05f, 0.05f);
	lightingShader.setUniform("dirLight.diffuse", 0.2f, 0.2f, 0.2f);
	lightingShader.setUniform("dirLight.specular", 0.5f, 0.5f, 0.5f);

	//point lights
	lightingShader.setUniform("pointLights[0].ambient", 0.05f, 0.05f, 0.05f);
	lightingShader.setUniform("pointLights[0].diffuse", 0.8f, 0.8f, 0.8f);
	lightingShader.setUniform("pointLights[0].specular", 1.0f, 1.0f, 1.0f);
	lightingShader.setUniform("pointLights[0].constant", 1.0f);
	lightingShader.setUniform("pointLights[0].linear", 0.09f);
	lightingShader.setUniform("pointLights[0].quadratic", 0.032f);

	//spot light
	lightingShader.setUniform("spotLight.constant", 1.0f);
	lightingShader.setUniform("spotLight.linear", 0.09f);
	lightingShader.setUniform("spotLight.quadratic", 0.032f);
	lightingShader.setUniform("spotLight.cutOff", glm::cos(glm::radians(12.5f)));
	lightingShader.setUniform("spotLight.outerCutOff", glm::cos(glm::radians(15.0f)));


	//game loop
	while (!glfwWindowShouldClose(m_Window)) {

		//************************* TIMING *************************

		//delta time
		GLfloat currentFrameTime = glfwGetTime();
		m_DeltaTime = currentFrameTime - m_LastFrameTime;
		m_LastFrameTime = currentFrameTime;

		//fps calculation
		m_FrameTimer += m_DeltaTime;
		m_Frames++;
		m_FramesSinceStart++;
		if (m_FrameTimer >= 1.0f) {
			std::cout << m_FpsCountText.c_str() << std::endl;
			m_FpsCountText = "ms per frame(average): " + std::to_string(1000.0 / double(m_Frames)) + "ms fps: " + std::to_string(m_Frames);
			m_FrameProblemsLastFrame = false;
			m_FrameTimer -= 1.0f;
			m_Frames = 0;
		}

		//events
		glfwPollEvents();

		//don't run this frame if delta time is larger then .2 seconds (means that it couldn't complete the frame in time)
		if (m_DeltaTime >= 0.2f) {
			m_FrameProblemsLastFrame = true;
			std::cout << "Did not run update/render, deltaTime was too large. There is a freeze" << std::endl;
			std::cout << m_FpsCountText.c_str() << std::endl;
			//m_FrameTimer = 0;
			//glfwSwapBuffers(window);
			continue;
		}

		//************************* UPDATE *************************

		//camera movement
		doCameraMovement();

		//lighting
		if (m_SpinningLight) {
			float range = 2 + sin(glfwGetTime()) / 2;
			m_LightPos.x = range * sin(glfwGetTime());
			m_LightPos.y = range * cos(glfwGetTime() / 4);
			m_LightPos.z = range * cos(glfwGetTime());
		} else {
			m_LightPos.x = 1.0f + sin(glfwGetTime()) * 2.0f;
			m_LightPos.y = sin(glfwGetTime() / 2.0f) * 1.0f;
		}

		//fade line position calculation
		lineHeight += 0.3f * m_DeltaTime;
		if (lineHeight >= 3.0f) {
			lineHeight -= 3.0f;
		}
		float upDownLineHeight = lineHeight < 1 ? lineHeight : -lineHeight + 2;
		if (lineHeight > 2.2) {
			upDownLineHeight = -0.2;
		}
		//wave update
		squareWave.update(m_DeltaTime);

		//general transform updates
		spinningNanosuit.rotate(glm::vec3(0.0f, 0.1f, 0.0f) * m_DeltaTime);

		movingNanosuit.translate(glm::vec3(0, 0, sin(glfwGetTime()) / 30), false);

		rotateAroundNanosuit.reset();
		rotateAroundNanosuit.translate(spinningNanosuit.getLocalPostion());
		rotateAroundNanosuit.scale(glm::vec3(0.2f));
		rotateAroundNanosuit.translate(glm::vec3(2, 0, 0));
		rotateAroundNanosuit.rotateAround(glm::vec3(0.0f, 2.0f*glfwGetTime(), 0.0f), rotateAroundNanosuit.getLocalPostion() * glm::vec3(0.5f));

		//************************* RENDER *************************

		///clear
		//set background color
		glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
		//clear last frame
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		//common mat4's
		glm::mat4 view;
		view = m_Camera.getViewMatrix();

		glm::mat4 projection;
		projection = glm::perspective(glm::radians(m_Camera.m_MouseZoom), (float) m_ScreenSize.x / m_ScreenSize.y, 0.1f, 100.0f);

		//************************* Normal render *************************

		///spinning nanosuit
		//if lighting is on then setup lighting data
		if (m_RealLighting) {
			lightingShader.use();

			//directional light
			//(done before main loop now)

			//point lights
			//(update light pos)
			lightingShader.setUniform("pointLights[0].position", m_LightPos.x, m_LightPos.y, m_LightPos.z);

			//spot light
			lightingShader.setUniform("spotLight.position", m_Camera.m_Position.x, m_Camera.m_Position.y, m_Camera.m_Position.z);
			lightingShader.setUniform("spotLight.direction", m_Camera.m_Front.x, m_Camera.m_Front.y, m_Camera.m_Front.z);
			if (m_FlashLightOn) {
				lightingShader.setUniform("spotLight.ambient", 0.0f, 0.0f, 0.0f);
				lightingShader.setUniform("spotLight.diffuse", 0.8f, 0.8f, 0.8f);
				lightingShader.setUniform("spotLight.specular", 1.0f, 1.0f, 1.0f);
			} else {
				lightingShader.setUniform("spotLight.ambient", 0.0f, 0.0f, 0.0f);
				lightingShader.setUniform("spotLight.diffuse", 0.0f, 0.0f, 0.0f);
				lightingShader.setUniform("spotLight.specular", 0.0f, 0.0f, 0.0f);
			}

		} else {
			basicShader.use();
		}


		Shader::setProjection(projection);
		Shader::setView(view);

		//draw nanosuit 1
		Shader::setModel(spinningNanosuit.getGlobalMatrix());
		nanosuitModel.draw();

		//draw nanosuit 2
		Shader::setModel(movingNanosuit.getGlobalMatrix());
		nanosuitModel.draw();

		///Fade Shader test
		//please note: anything drawn below this while this is transparent will now draw
		fadeShader.use();
		fadeShader.setProjection(projection);
		fadeShader.setView(view);

		//tell shader about line pos and screen size
		fadeShader.setUniform("screenSize", m_ScreenSize.x, m_ScreenSize.y);
		fadeShader.setUniform("height", upDownLineHeight);

		fadeShader.setModel(rotateAroundNanosuit.getGlobalMatrix());
		//nanosuitModel.draw();

		///Wave Draw
		solidColorShader.use();

		solidColorShader.setProjection(projection);
		solidColorShader.setView(view);
		squareWave.draw();

		///Light
		//draw light if lighting is on
		if (m_RealLighting) {
			//LIGHT OBJECT RENDER
			lampShader.use();

			//set up pos
			glm::mat4 pos;
			pos = glm::translate(pos, m_LightPos);
			pos = glm::scale(pos, glm::vec3(0.2f));

			lampShader.setModel(pos);
			lampShader.setProjection(projection);
			lampShader.setView(view);

			lampShader.setUniform("Color", 1.0f, 1.0f, 1.0f);

			box.draw();
		}

		//************************* UI RENDER *************************
		///screen setup 
#if UI_USES_SCREEN_SIZE
		projection = glm::ortho(0.0f, m_ScreenSize.x, m_ScreenSize.y, 0.0f);
#else
		projection = glm::ortho(0.0f, 800.0f, 600.0f, 0.0f);
#endif // UI_USES_SCREEN_SIZE == true


		//glDepthMask(GL_FALSE); //disable this to have things drawn later on top (uncomment after UI RENDER
		glDisable(GL_DEPTH_TEST);


		///Text draw
		textShader.use();

		textShader.setProjection(projection);

		if (m_FrameProblemsLastFrame) {
			font.draw(m_FpsCountText.c_str(), 0, 0, 1, glm::vec3(1.0f, 0.0f, 0.0f));
		} else {
			font.draw(m_FpsCountText.c_str(), 0, 0);
		}

		//font.draw("Controls:\nEnter - Lighting\nF - Flashlight\nF1 - Render modes", 0, 30);

		//************************* UI RENDER END *************************
		//glDepthMask(GL_TRUE);
		glEnable(GL_DEPTH_TEST);

		//swap the buffers to show image on screen
		glfwSwapBuffers(m_Window);

	}

	//exit
	glfwTerminate();

	return 0;
}