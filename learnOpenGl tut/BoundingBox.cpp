#include "BoundingBox.h"

#include "Transform.h"

BoundingBox::BoundingBox(Shape * a_Parent) {
	m_Parent = a_Parent;
}

BoundingBox::~BoundingBox() {
}

void BoundingBox::draw() {
	m_Cube.drawWireframe();
}

void BoundingBox::update() {
	m_BackBotLeft = m_FrontTopRight = m_Parent->m_Transform->getGlobalPosition();
	std::vector<Shape::Vertex>* verts = &m_Parent->m_Vertices;
	for (int i = 0; i < verts->size(); i++) {
		glm::vec3 vert = m_Parent->m_Transform->getGlobalPosition(verts->at(i).Position);

		if (vert.x > m_FrontTopRight.x) {
			m_FrontTopRight.x = vert.x;
		}
		if (vert.y > m_FrontTopRight.y) {
			m_FrontTopRight.y = vert.y;
		}
		if (vert.z > m_FrontTopRight.z) {
			m_FrontTopRight.z = vert.z;
		}

		if (vert.x < m_BackBotLeft.x) {
			m_BackBotLeft.x = vert.x;
		}
		if (vert.y < m_BackBotLeft.y) {
			m_BackBotLeft.y = vert.y;
		}
		if (vert.z < m_BackBotLeft.z) {
			m_BackBotLeft.z = vert.z;
		}
	}
	m_Cube.setPosition(Cube::Points::FrontTopRight, m_FrontTopRight);
	m_Cube.setPosition(Cube::Points::BackBotLeft, m_BackBotLeft);

	m_Cube.setPosition(Cube::Points::BackBotRight, glm::vec3(m_FrontTopRight.x, m_BackBotLeft.y, m_BackBotLeft.z));
	m_Cube.setPosition(Cube::Points::BackTopLeft, glm::vec3(m_BackBotLeft.x, m_FrontTopRight.y, m_BackBotLeft.z));
	m_Cube.setPosition(Cube::Points::BackTopRight, glm::vec3(m_FrontTopRight.x, m_FrontTopRight.y, m_BackBotLeft.z));

	m_Cube.setPosition(Cube::Points::FrontTopLeft, glm::vec3(m_BackBotLeft.x, m_FrontTopRight.y, m_FrontTopRight.z));
	m_Cube.setPosition(Cube::Points::FrontBotRight, glm::vec3(m_FrontTopRight.x, m_BackBotLeft.y, m_FrontTopRight.z));
	m_Cube.setPosition(Cube::Points::FrontBotLeft, glm::vec3(m_BackBotLeft.x, m_BackBotLeft.y, m_FrontTopRight.z));
}
