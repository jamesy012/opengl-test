#pragma once

#include <glm\glm.hpp>

#include "Shape.h"
#include "Cube.h"

class BoundingBox {
public:
	BoundingBox(Shape* a_Parent);
	~BoundingBox();

	void draw();
	void update();
private:
	Cube m_Cube;
	
	glm::vec3 m_BackBotLeft;
	glm::vec3 m_FrontTopRight;

	Shape* m_Parent;
};

