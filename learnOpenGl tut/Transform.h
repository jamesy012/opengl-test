#pragma once

#include <glm\glm.hpp>
#include <glm/gtc/quaternion.hpp> 
#include <glm/gtx/quaternion.hpp>


#include <vector>

using namespace glm;

class Transform {
public:
	Transform();
	Transform(const Transform& a_Copy);

	void setDirty();
	void reset();

	void translate(vec3 a_Translation, bool a_World = true);
	void scale(vec3 a_Scale);
	void rotate(vec3 a_Rotation);
	void rotate(quat a_Rotation);
	void rotateAround(vec3 a_Rotation, vec3 a_pivot);
	void rotateAround(vec3 a_Rotation);

	mat4 getGlobalMatrix();

	vec3 getGlobalPosition();
	vec3 getGlobalPosition(vec3 a_Local);

	vec3 getLocalPostion();
	vec3 getLocalScale();
	quat getLocalRoation();

	vec3 getForward();
	vec3 getRight();
	vec3 getUp();

	void updateTransforms();

	void setParent(Transform* a_Parent);
private:
	vec3 m_LocalPosition;
	quat m_LocalRoation;
	vec3 m_LocalScale;

	mat4 m_LocalMatrix;
	mat4 m_GlobalMatrix;

	bool m_IsDirty;

	Transform* m_Parent;
	std::vector<Transform*> m_Children;
};

