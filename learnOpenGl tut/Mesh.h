#pragma once

#include <vector>

#include "Shape.h"

class Mesh : public Shape {
public:
	Mesh();
	~Mesh();

	void setMesh(std::vector<Vertex> a_Vertices, std::vector<GLuint> a_Indices, std::vector<Texture> a_Textures);

	void draw();
};

