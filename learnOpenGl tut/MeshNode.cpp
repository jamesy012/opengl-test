#include "MeshNode.h"

#include <iostream>

#include "Mesh.h"


MeshNode::MeshNode() {
	m_Parent = nullptr;
}


MeshNode::~MeshNode() {
	for (int i = 0; i < m_Children.size(); i++) {
		delete m_Children[i];
	}
	for (int i = 0; i < m_Meshes.size();i++) {
		delete m_Meshes[i];
	}
}

void MeshNode::addChild(MeshNode * a_Child) {
	if (a_Child->hasParent()) {
		std::cout << "cant add child since child already has parent" << std::endl;
		return;
	}
	m_Children.push_back(a_Child);
}

void MeshNode::setParent(MeshNode * a_Parent) {
	a_Parent->addChild(this);
}

bool MeshNode::hasParent() {
	return m_Parent != nullptr;
}

void MeshNode::addMesh(Mesh * a_Mesh) {
	m_Meshes.push_back(a_Mesh);
}

void MeshNode::draw() {
	for (int i = 0; i < m_Meshes.size(); i++) {
		m_Meshes[i]->draw();
	}
	for (int i = 0; i < m_Children.size(); i++) {
		m_Children[i]->draw();
	}
}
