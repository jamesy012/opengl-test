#include "Camera.h"


Camera::Camera(glm::vec3 a_Position, glm::vec3 a_Up, GLfloat a_Yaw, GLfloat a_Pitch) {
	m_Front = glm::vec3(0, 0, -1);
	m_MovementSpeed = SPEED;
	m_MouseSensitivity = SENSITIVTY;
	m_MouseZoom = ZOOM;

	m_Position = a_Position;
	m_WorldUp = a_Up;
	m_Yaw = a_Yaw;
	m_Pitch = a_Pitch;

	updateCameraVectors();
}

glm::mat4 Camera::getViewMatrix() {
	//glm::vec3 d = glm::normalize(m_Position - m_Front);
	//glm::vec3 r = glm::normalize(glm::cross(glm::normalize(m_WorldUp), d));
	//glm::vec3 u = glm::cross(d,r);
	//glm::vec3 p = m_Position;
	//glm::mat4 direction =
	//	glm::mat4(r.x, r.y, r.z, 0,
	//			  u.x, u.y, u.z, 0,
	//			  d.x, d.y, d.z, 0,
	//			  0, 0, 0, 1);
	//glm::mat4 position = glm::mat4(1, 0, 0, -p.x, 0, 1, 0, -p.y, 0, 0, 1, -p.z, 0, 0, 0, 1);
	//return direction * position;
	return glm::lookAt(m_Position, m_Position + m_Front, m_Up);
}

void Camera::updateMovement(Camera_Movement a_Dir, GLfloat a_DeltaTime) {
	GLfloat vel = m_MovementSpeed * a_DeltaTime;
	glm::vec3 movment;
	if (a_Dir == FORWARD) {
		movment += m_Front * vel;
	}
	if (a_Dir == BACKWARD) {
		movment -= m_Front * vel;
	}
	if (a_Dir == LEFT) {
		movment -= m_Right * vel;
	}
	if (a_Dir == RIGHT) {
		movment += m_Right* vel;
	}
	if (a_Dir == UP) {
		movment += m_Up* vel;
	}
	if (a_Dir == DOWN) {
		movment -= m_Up* vel;
	}
	//movment.y = 0;
	m_Position += movment;
}

void Camera::updateMouse(GLfloat a_XOffset, GLfloat a_YOffset, GLboolean a_ConstrainPitch) {
	a_XOffset *= m_MouseSensitivity;
	a_YOffset *= m_MouseSensitivity;

	m_Yaw += a_XOffset;
	m_Pitch += a_YOffset;


	if (a_ConstrainPitch) {
		if (m_Pitch > 89.0f)
			m_Pitch = 89.0f;
		if (m_Pitch < -89.0f)
			m_Pitch = -89.0f;
	}

	if (m_Yaw >= 360) {
		m_Yaw -= 360;
	}
	if (m_Yaw <= -360) {
		m_Yaw += 360;
	}

	updateCameraVectors();
}

void Camera::updateZoom(GLfloat a_YOffset) {
	m_MouseZoom -= a_YOffset;
	if (m_MouseZoom <= 1.0f) {
		m_MouseZoom = 1.0f;
	}
	if (m_MouseZoom >= 45.0f) {
		m_MouseZoom = 45.0f;
	}

}

void Camera::updateCameraVectors() {

	glm::vec3 front;
	front.x = glm::cos(glm::radians(m_Yaw)) * cos(glm::radians(m_Pitch));
	front.y = glm::sin(glm::radians(m_Pitch));
	front.z = glm::sin(glm::radians(m_Yaw)) * cos(glm::radians(m_Pitch));
	m_Front = glm::normalize(front);

	m_Right = glm::normalize(glm::cross(m_Front, m_WorldUp));
	m_Up = glm::normalize(glm::cross(m_Right, m_Front));
}
