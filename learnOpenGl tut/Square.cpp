#include "Square.h"


Square::Square() {



	//0 - top right
	//1 - bot right
	//2 - bot left
	//3 - top left
	m_Indices.resize(6);
	m_Vertices.resize(4);

	//set up indices
	m_Indices[0] = 3;
	m_Indices[1] = 1;
	m_Indices[2] = 0;

	m_Indices[3] = 3;
	m_Indices[4] = 2;
	m_Indices[5] = 1;

	//set up default square
	m_Vertices[0] = { glm::vec3(1.0f, 1.0f, 0.0f), glm::vec3(1), glm::vec2(1.0f, 1.0f) };
	m_Vertices[1] = { glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(1), glm::vec2(1.0f, 0.0f) };
	m_Vertices[2] = { glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1), glm::vec2(0.0f, 0.0f) };
	m_Vertices[3] = { glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1), glm::vec2(0.0f, 1.0f) };

	setupMesh();
}


Square::~Square() {
}

void Square::setTopRight(glm::vec3 a_Pos) {
	setDirty();
	m_Vertices[0].Position = a_Pos;
}

void Square::setTopLeft(glm::vec3 a_Pos) {
	setDirty();
	m_Vertices[3].Position = a_Pos;
}

void Square::setBotRight(glm::vec3 a_Pos) {
	setDirty();
	m_Vertices[1].Position = a_Pos;
}

void Square::setBotLeft(glm::vec3 a_Pos) {
	setDirty();
	m_Vertices[2].Position = a_Pos;
}

void Square::setTop(GLfloat a_Y) {
	setDirty();
	m_Vertices[0].Position.y = a_Y;
	m_Vertices[3].Position.y = a_Y;
}

void Square::setBot(GLfloat a_Y) {
	setDirty();
	m_Vertices[1].Position.y = -a_Y;
	m_Vertices[2].Position.y = -a_Y;
}

void Square::setLeft(GLfloat a_X) {
	setDirty();
	m_Vertices[2].Position.x = -a_X;
	m_Vertices[3].Position.x = -a_X;
}

void Square::setRight(GLfloat a_X) {
	setDirty();
	m_Vertices[0].Position.x = a_X;
	m_Vertices[1].Position.x = a_X;
}

glm::vec3 Square::getBotLeft() {
	return m_Vertices[2].Position;
}

glm::vec3 Square::getBotRight() {
	return m_Vertices[1].Position;
}

glm::vec3 Square::getTopLeft() {
	return m_Vertices[3].Position;
}

glm::vec3 Square::getTopRight() {
	return m_Vertices[0].Position;
}