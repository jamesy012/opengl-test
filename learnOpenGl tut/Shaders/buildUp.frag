#version 330 core

in vec2 TexCoords;

out vec4 color;

uniform sampler2D texture_diffuse1;

uniform vec2 screenSize;
uniform float height = 0.5f;
float diff = 0.05f;

void main() {  
	int type = 0;

	if(type == 0){
		vec4 col = vec4(texture(texture_diffuse1, TexCoords));
		if(TexCoords.y> height + diff){
				col.w = TexCoords.y*height;
		}  else
		
		if(TexCoords.y > height){
				col = vec4(0.01f,0.01f,1.0f,0.5f) * col;
		}
		if(col.w == 0){
			discard;
		}

		color = col;
		return;
	}else

		if(type == 1){
		if(TexCoords.y> height + diff){
				discard;
		}  else
		
		if(TexCoords.y > height){
				color = vec4(0.0f,0.0f,1.0f,0.5f);
				return;
		}
	}else

	if(type == 2){
		if(gl_FragCoord.y / screenSize.y  > height + diff){
				discard;
			return;
		}  
		
		if(gl_FragCoord.y / screenSize.y  > height){
				color = vec4(0.0f,0.0f,1.0f,0.5f);
			return;
		}
	}

//if(100 > gl_FragCoord.y){
//	discard;
//	return;
//}
//
//if(150 > gl_FragCoord.y){
//	color = vec4(0.0f,0.0f,1.0f,0.5f);
//	return;
//}
	
    color = vec4(texture(texture_diffuse1, TexCoords));
}