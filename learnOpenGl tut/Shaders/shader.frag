#version 330 core

struct Material {

	//sampler2D emission;
	float shininess;
};

struct PointLight {
	vec3 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;
};

struct SpotLight {
	vec3 position;
	vec3 direction;
	float cutOff;
	float outerCutOff;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;
};

struct DirectionalLight{
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

out vec4 color;

in vec3 FragPos;  
in vec3 Normal;  
in vec2 TexCoords;
  
uniform vec3 viewPos;
uniform Material material;

uniform sampler2D texture_diffuse1;
//uniform sampler2D texture_diffuse2;
//uniform sampler2D texture_diffuse3;
uniform sampler2D texture_specular1;
//uniform sampler2D texture_specular2;

#define NR_POINT_LIGHTS 1
uniform PointLight pointLights[NR_POINT_LIGHTS];

uniform DirectionalLight dirLight;
uniform SpotLight spotLight;

vec3 CalculatePointLight(PointLight light, vec3 normal, vec3 viewDir);
vec3 CalculateSpotLight(SpotLight light, vec3 normal, vec3 viewDir);
vec3 CalculateDirLight(DirectionalLight light, vec3 normal, vec3 viewDir);

void main() {        
	vec3 norm = normalize(Normal);
	vec3 viewDir = normalize(viewPos - FragPos);

    vec3 result;

	//dir
	result = CalculateDirLight(dirLight, norm, viewDir);

	//point
	for(int i = 0;i<NR_POINT_LIGHTS;i++){
		result += CalculatePointLight(pointLights[i], norm, viewDir);
	}
	
	//spot
	result += CalculateSpotLight(spotLight, norm, viewDir);

	float bits = 32.0f;
	int red = int(result.r * bits);
	int green = int(result.g * bits);
	int blue = int(result.b * bits);
	result.r = float(red / bits);
	result.g = float(green / bits);
	result.b = float(blue / bits);


    color = vec4(result, 1.0f);
} 

vec3 CalculatePointLight(PointLight light, vec3 normal, vec3 viewDir) {
    vec3 lightDir = normalize(light.position - FragPos);
  	
    // Diffuse 
	float diff = max(dot(normal, lightDir), 0.0);
    
    // Specular
    vec3 reflectDir = reflect(-lightDir, normal);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

	//attenuation
	float dist    = length(light.position - FragPos);
	float attenuation = 1.0f / (light.constant + light.linear * dist + 
    		    light.quadratic * (dist * dist));    

    // Ambient
    vec3 ambient  = light.ambient  * vec3(texture(texture_diffuse1, TexCoords));
    vec3 diffuse  = light.diffuse  * diff * vec3(texture(texture_diffuse1, TexCoords));  
    vec3 specular = light.specular * spec * vec3(texture(texture_specular1, TexCoords));
	ambient  *= attenuation;
	diffuse  *= attenuation;
	specular *= attenuation;

	//vec3 emission = vec3(texture(material.emission, TexCoords));
    //    
    //return (ambient + diffuse + specular + emission);

	return (ambient + diffuse + specular);
}

vec3 CalculateSpotLight(SpotLight light, vec3 normal, vec3 viewDir) {
    vec3 lightDir = normalize(light.position - FragPos);
  	
    // Diffuse 
    float diff = max(dot(normal, lightDir), 0.0);
    
    // Specular
    vec3 reflectDir = reflect(-lightDir, normal);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

	//attenuation / falloff
	float dist    = length(light.position - FragPos);
	float attenuation = 1.0f / (light.constant + light.linear * dist + 
    		    light.quadratic * (dist * dist));   
				
	//make it circle 
	float theta = dot(lightDir, normalize(-light.direction));
	float epsilon = light.cutOff - light.outerCutOff;
	float intensity =  clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0f);

    // Ambient
    vec3 ambient = light.ambient * vec3(texture(texture_diffuse1, TexCoords));
    vec3 diffuse = light.diffuse * diff * vec3(texture(texture_diffuse1, TexCoords));  
    vec3 specular = light.specular * spec * vec3(texture(texture_specular1, TexCoords));
	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;

	diffuse *= intensity;
	specular *= intensity;

	return (ambient + diffuse + specular);
}

vec3 CalculateDirLight(DirectionalLight light, vec3 normal, vec3 viewDir) {
    vec3 lightDir = normalize(-light.direction);

    // Diffuse 
    float diff = max(dot(normal, lightDir), 0.0);

    // Specular
    vec3 reflectDir = reflect(-lightDir, normal);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

    // results
    vec3 ambient = light.ambient * vec3(texture(texture_diffuse1, TexCoords));
    vec3 diffuse = light.diffuse * diff * vec3(texture(texture_diffuse1, TexCoords));  
    vec3 specular = light.specular * spec * vec3(texture(texture_specular1, TexCoords));

	return (ambient + diffuse + specular);
}