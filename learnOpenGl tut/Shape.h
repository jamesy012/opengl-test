#pragma once

#include <glm\glm.hpp>
#include <gl\glew.h>
#include <vector>

#include "Object.h"

class BoundingBox;



class Shape : public Object {
	friend class BoundingBox;
public:
	struct Vertex {
		glm::vec3 Position;
		glm::vec3 Normal;
		glm::vec2 TexCoords;
	};

	struct Texture {
		GLuint Id;
		std::string Type;
		std::string Path;
	};
	glm::vec4 m_Color;

	Shape();
	~Shape();

	virtual void draw();
	void drawWireframe();
	void drawBoundingBox();

	void createBoundingBox();

protected:

	std::vector<GLuint> m_Indices;
	std::vector<Vertex> m_Vertices;
	std::vector<Texture> m_Textures;

	void setDirty();
	void updateMesh();
	void setupMesh();

	void loadTextures();

private:
	GLuint VAO, VBO, EBO;
	bool m_IsDirty = false;
	BoundingBox* m_BoundingBox;

	void checkDirty();

	void unloadTextures();
};

