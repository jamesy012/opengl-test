#include "Transform.h"

#include <glm\gtc\matrix_transform.hpp>


Transform::Transform() {
	m_Parent = nullptr;
	reset();
	updateTransforms();
}

Transform::Transform(const Transform & a_Copy) {
	m_IsDirty = a_Copy.m_IsDirty;
	m_LocalPosition = a_Copy.m_LocalPosition;
	m_LocalRoation = a_Copy.m_LocalRoation;
	m_LocalScale = a_Copy.m_LocalScale;
	setParent(a_Copy.m_Parent);
}

void Transform::setDirty() {
	m_IsDirty = true;

	////set all children as dirty
	//for (int i = 0; i < m_Children.size(); i++) {
	//	m_Children[i]->setDirty();
	//}
}

void Transform::reset() {
	m_LocalPosition = vec3();
	m_LocalScale = vec3(1.0f);
	m_LocalRoation = quat();
	m_LocalMatrix = m_GlobalMatrix = mat4();
	setDirty();
}

void Transform::translate(vec3 a_Translation, bool a_World) {
	if (a_World) {
		m_LocalPosition += a_Translation;
	} else {
		m_LocalPosition += m_LocalRoation  * a_Translation;
	}
	setDirty();
}

void Transform::scale(vec3 a_Scale) {
	m_LocalScale = a_Scale;
	setDirty();
}

void Transform::rotate(vec3 a_Rotation) {
	m_LocalRoation *= quat(a_Rotation);
	setDirty();
}

void Transform::rotate(quat a_Rotation) {
	m_LocalRoation *= a_Rotation;
	setDirty();
}

void Transform::rotateAround(vec3 a_Rotation, vec3 a_pivot) {
	vec3 dir = m_LocalPosition - a_pivot; // get point direction relative to pivot
	dir = quat(a_Rotation) * dir;
	m_LocalPosition = dir + a_pivot;
}

void Transform::rotateAround(vec3 a_Rotation) {
	vec3 pivot = vec3();
	if (m_Parent) {
		pivot = m_Parent->getLocalPostion();
	}
	vec3 dir = m_LocalPosition - pivot; // get point direction relative to pivot
	dir = quat(a_Rotation) * dir;
	m_LocalPosition = dir;
}

mat4 Transform::getGlobalMatrix() {
	updateTransforms();
	
	return m_GlobalMatrix;
}

vec3 Transform::getGlobalPosition() {
	vec4 temp = vec4(m_LocalPosition.x, m_LocalPosition.y, m_LocalPosition.z, 1);

	temp = m_GlobalMatrix * temp;

	vec3 toVec3 = vec3(temp.x, temp.y, temp.z);
	return  toVec3;
}

vec3 Transform::getGlobalPosition(vec3 a_Local) {
	vec4 temp = vec4(a_Local.x, a_Local.y, a_Local.z, 1);

	temp = m_GlobalMatrix * temp;

	vec3 toVec3 = vec3(temp.x, temp.y, temp.z);
	return  toVec3;
}

vec3 Transform::getLocalPostion() {
	return m_LocalPosition;
}

vec3 Transform::getLocalScale() {
	return m_LocalScale;
}

quat Transform::getLocalRoation() {
	return m_LocalRoation;
}

vec3 Transform::getForward() {
	updateTransforms();

	return glm::normalize(m_LocalMatrix * vec4(0, 0, 1, 0));
}

vec3 Transform::getRight() {
	updateTransforms();

	return  glm::normalize(m_LocalMatrix * vec4(1, 0, 0, 0));
}

vec3 Transform::getUp() {
	updateTransforms();

	return glm::normalize(m_LocalMatrix * vec4(0, 1, 0, 0));
}

void Transform::updateTransforms() {
	if (m_IsDirty) {
		m_LocalMatrix = mat4(1.0f);
		mat4 rotationMatrix = toMat4(m_LocalRoation);
		//m_LocalMatrix = glm::rotate(m_LocalMatrix, m_LocalRoation.x, vec3(1, 0, 0));
		//m_LocalMatrix = glm::rotate(m_LocalMatrix, m_LocalRoation.y, vec3(0, 1, 0));
		//m_LocalMatrix = glm::rotate(m_LocalMatrix, m_LocalRoation.z, vec3(0, 0, 1));
		m_LocalMatrix = glm::translate(m_LocalMatrix, m_LocalPosition);
		m_LocalMatrix *= rotationMatrix;// = Pos * rot * scale
		m_LocalMatrix = glm::scale(m_LocalMatrix, m_LocalScale);
		m_IsDirty = false;

		//set all children as dirty
		for (int i = 0; i < m_Children.size(); i++) {
			m_Children[i]->updateTransforms();
		}

	}


	if (m_Parent == nullptr) {
		m_GlobalMatrix = m_LocalMatrix;
	} else {
		m_GlobalMatrix = m_Parent->getGlobalMatrix() * m_LocalMatrix;
	}
}

void Transform::setParent(Transform * a_Parent) {
	m_Parent = a_Parent;
	m_IsDirty = true;;
}
