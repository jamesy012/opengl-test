#include "Cube.h"



Cube::Cube() {

	m_Indices = {
		0, 1, 2,
		2, 3, 0,
		// top
		1, 5, 6,
		6, 2, 1,
		// back
		7, 6, 5,
		5, 4, 7,
		// bottom
		4, 0, 3,
		3, 7, 4,
		// left
		4, 5, 1,
		1, 0, 4,
		// right
		3, 2, 6,
		6, 7, 3
	};

	m_Vertices = {
		//front
		{ glm::vec3(-1.0f, -1.0f,  1.0f), glm::vec3(1), glm::vec2(1.0f, 1.0f) },//front bot left
		{ glm::vec3( 1.0f, -1.0f,  1.0f), glm::vec3(1), glm::vec2(1.0f, 1.0f) },//front bot right
		{ glm::vec3( 1.0f,  1.0f,  1.0f), glm::vec3(1), glm::vec2(1.0f, 1.0f) },//front top right
		{ glm::vec3(-1.0f,  1.0f,  1.0f), glm::vec3(1), glm::vec2(1.0f, 1.0f) },//front top left
		//back
		{ glm::vec3(-1.0f, -1.0f, -1.0f), glm::vec3(1), glm::vec2(1.0f, 1.0f) },//back bot left
		{ glm::vec3( 1.0f, -1.0f, -1.0f), glm::vec3(1), glm::vec2(1.0f, 1.0f) },//back bot right
		{ glm::vec3( 1.0f,  1.0f, -1.0f), glm::vec3(1), glm::vec2(1.0f, 1.0f) },//back top right
		{ glm::vec3(-1.0f,  1.0f, -1.0f), glm::vec3(1), glm::vec2(1.0f, 1.0f) } //back top left

	};

	for (int i = 0; i < m_Vertices.size(); i++) {
		m_Vertices[i].Position = m_Vertices[i].Position * 0.5f;
	}

	setupMesh();
}


Cube::~Cube() {
}

void Cube::setPosition(Points a_Point, glm::vec3 a_Pos) {
	setDirty();
	m_Vertices[a_Point].Position = a_Pos;
}
