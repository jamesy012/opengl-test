#pragma once

#include <glm\glm.hpp>

#include "Shape.h"

class Square : public Shape {
public:
	Square();
	~Square();

	void setTopRight(glm::vec3 a_Pos);
	void setTopLeft(glm::vec3 a_Pos);
	void setBotRight(glm::vec3 a_Pos);
	void setBotLeft(glm::vec3 a_Pos);
	void setTop(GLfloat a_Y);
	void setBot(GLfloat a_Y);
	void setLeft(GLfloat a_X);
	void setRight(GLfloat a_X);

	glm::vec3 getBotLeft();
	glm::vec3 getBotRight();
	glm::vec3 getTopLeft();
	glm::vec3 getTopRight();



};

