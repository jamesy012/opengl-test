#include "Mesh.h"

#include "Shader.h"

Mesh::Mesh() {
}

Mesh::~Mesh() {
}

void Mesh::setMesh(std::vector<Vertex> a_Vertices, std::vector<GLuint> a_Indices, std::vector<Texture> a_Textures) {
	this->m_Vertices = a_Vertices;
	this->m_Indices = a_Indices;
	this->m_Textures = a_Textures;

	setupMesh();
	createBoundingBox();
}

void Mesh::draw() {

	// Also set each mesh's shininess property to a default value (if you want you could extend this to another mesh property and possibly change this value)
	Shader::setUniform("material.shininess", 128.0f);

	// Draw mesh
	Shape::draw();

	drawBoundingBox();
}