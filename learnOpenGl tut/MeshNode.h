#pragma once

#include <vector>

class Mesh;
class Shader;

class MeshNode {
public:
	MeshNode();
	~MeshNode();

	void addChild(MeshNode* a_Child);
	void setParent(MeshNode* a_Parent);

	bool hasParent();

	void addMesh(Mesh* a_Mesh);

	void draw();

private:
	MeshNode* m_Parent;
	std::vector<MeshNode*> m_Children;

	std::vector<Mesh*> m_Meshes;
};

