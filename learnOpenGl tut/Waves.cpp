#include "Waves.h"

#include <GLFW\glfw3.h>

#include "Transform.h"
#include "Square.h"
#include "Shader.h";

Waves::Waves(GLuint a_Lines, GLfloat a_WaveWidth, GLboolean a_BothSides) {
	m_WaveLength = a_WaveWidth;
	m_SquareWidth = a_WaveWidth / a_Lines;
	m_NumLines = a_Lines;
	m_BothSides = a_BothSides;

	m_Squares = new WaterBar[a_Lines];

	m_Transform = new Transform();


	for (int i = 0; i < a_Lines; i++) {
		m_Squares[i].square.setRight(m_SquareWidth);
		m_Squares[i].square.m_Transform->translate(glm::vec3(m_SquareWidth*i, 0, 0));
		m_Squares[i].square.m_Transform->setParent(m_Transform);
		m_Squares[i].height = Y_OFFSET;
		m_Squares[i].heightDiff = 0;
		m_Squares[i].speed = 0;
	}
	m_Squares[0].square.createBoundingBox();

	m_Waves = new Wave[NUM_OF_WAVES];
	for (int w = 0; w < NUM_OF_WAVES; w++) {
		m_Waves[w].amplitude = 0.03f + randomValue() * 0.30;
		if (m_BothSides) {
			m_Waves[w].amplitude *= 2.0f;
		}
		m_Waves[w].offset = rand() % 1000;
		m_Waves[w].speed = -10.0f + (randomValue() * 20.0f);
		m_Waves[w].waveLength = 0.5f + randomValue() * 10.0f;
	}

	//wave.amplitude = 0.08f;
	//wave.offset = 0;
	//wave.speed = 3.0f;
	//wave.waveLength = 3.6f;
	//
	//wave2.amplitude = 0.03f;
	//wave2.offset = 0;
	//wave2.speed = 10.0f;
	//wave2.waveLength = 1.0f;
}

float Waves::randomValue() {
	return (rand() % 100) / 100.0f;
}


Waves::~Waves() {
	delete[] m_Squares;
	delete[] m_Waves;
	delete m_Transform;
}

void Waves::update(float a_DeltaTime) {
	GLfloat time = glfwGetTime();
	for (int i = 0; i < m_NumLines; i++) {
		if (m_BothSides) {
			m_Squares[i].square.m_Transform->translate(glm::vec3(0, -m_Squares[i].heightDiff / 2, 0));
		}
		m_Squares[i].speed *= 0.90f;

		float sinWave = 0;

		//m_Squares[i].speed += sin((i + glfwGetTime())/WAVE_SPEED) * 0.08f;

		//sinWave += sin((wave.offset + i + glfwGetTime()*wave.speed)/wave.waveLength)* wave.amplitude;
		//sinWave += sin((wave2.offset + i + glfwGetTime()*wave2.speed) / wave2.waveLength)* wave2.amplitude;
		//sinWave /= 2;

		for (int w = 0; w < NUM_OF_WAVES; w++) {
			sinWave += sin((m_Waves[w].offset + i + time*m_Waves[w].speed) / m_Waves[w].waveLength)* m_Waves[w].amplitude;
		}
		sinWave /= NUM_OF_WAVES;

		m_Squares[i].speed += sinWave;

		float diff = Y_OFFSET - m_Squares[i].height;
		diff *= 0.6f;
		//if (diff > 0.5f) {
		//	diff = 0.5f;
		//}
		//if (diff < -0.5f) {
		//	diff = -0.5f;
		//}
		m_Squares[i].speed += diff;

		//if (m_Squares[i].height > Y_OFFSET) {
		//	m_Squares[i].speed -= DAMPENING;
		//}
		//if (m_Squares[i].height < Y_OFFSET) {
		//	m_Squares[i].speed += DAMPENING;
		//}

	}

	//m_Squares[1].speed += (m_Squares[0].height - m_Squares[1].height) * 0.5f;
	//m_Squares[1].speed += (m_Squares[2].height - m_Squares[1].height) * 0.5f;
	//for (int q = 0; q < 2; q++) {
	for (int i = 0; i < m_NumLines; i++) {
		if (i == 0) {
			m_Squares[i].speed += (m_Squares[m_NumLines - 1].height - m_Squares[i].height) * DAMPENING;
		} else {
			m_Squares[i].speed += (m_Squares[i - 1].height - m_Squares[i].height) * DAMPENING;
		}

		if (i == m_NumLines - 1) {
			m_Squares[i].speed += (m_Squares[0].height - m_Squares[i].height) * DAMPENING;
		} else {
			m_Squares[i].speed += (m_Squares[i + 1].height - m_Squares[i].height) * DAMPENING;
		}
	}
	//}

	updateBars();
	if (!m_BothSides) {
		linkBars();
	}
}

void Waves::draw() {
	for (int i = 0; i < m_NumLines; i++) {
		Shader::setModel(m_Squares[i].square.m_Transform->getGlobalMatrix());
		m_Squares[i].square.draw();
	}
	Shader::setModel(m_Squares[0].square.m_Transform->getGlobalMatrix());
	m_Squares[0].square.drawBoundingBox();
}

void Waves::linkBars() {
	m_Squares[0].square.setTopLeft(glm::vec3(0, m_Squares[m_NumLines - 1].square.getTopRight().y, 0));
	for (int i = 1; i < m_NumLines; i++) {
		m_Squares[i].square.setTopLeft(glm::vec3(0, m_Squares[i - 1].square.getTopRight().y, 0));
	}
}

void Waves::updateBars() {
	m_MaxHeight = m_MinHeight = 0;
	for (int i = 0; i < m_NumLines; i++) {

		GLfloat lastHeight = m_Squares[i].height;
		m_Squares[i].height += m_Squares[i].speed;
		m_Squares[i].heightDiff = m_Squares[i].height - lastHeight;

		if (m_Squares[i].height > m_MaxHeight) {
			m_MaxHeight = m_Squares[i].height;
		}
		if (m_Squares[i].height < m_MinHeight) {
			m_MinHeight = m_Squares[i].height;
		}

		m_Squares[i].square.setTop(m_Squares[i].height);

	}

	for (int i = 0; i < m_NumLines; i++) {
		float diff = 0;
		if (m_Squares[i].height >= 0) {
			diff = m_Squares[i].height / m_MaxHeight;
			m_Squares[i].square.m_Color = glm::vec4(0.0f, 1.0f, diff, 1.0f);
		} else {
			diff = -m_Squares[i].height / -m_MinHeight;
			m_Squares[i].square.m_Color = glm::vec4(0.0f, diff, 1.0f, 1.0f);
		}
	}
}
